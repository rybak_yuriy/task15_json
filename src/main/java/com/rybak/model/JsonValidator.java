package com.rybak.model;

import org.everit.json.schema.Schema;
import org.everit.json.schema.ValidationException;
import org.everit.json.schema.loader.SchemaLoader;
import org.json.JSONObject;
import org.json.JSONTokener;

import java.io.FileNotFoundException;
import java.io.FileReader;

public class JsonValidator {
    public static boolean validate(String validator, String json) throws FileNotFoundException {
        JSONObject jsonSchema = new JSONObject(
                new JSONTokener(new FileReader(validator)));
        JSONObject jsonSubject = new JSONObject(
                new JSONTokener(new FileReader(json)));
        Schema schema = SchemaLoader.load(jsonSchema);
        try {
            schema.validate(jsonSubject);
        } catch (ValidationException e) {
            e.printStackTrace();
            return false;
        }
        return true;
    }
}

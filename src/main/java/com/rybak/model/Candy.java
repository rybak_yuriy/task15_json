package com.rybak.model;

public class Candy {

    private String name;
    private int energy;
    private String type;
    private Ingredient ingredients;
    private Value value;
    private String production;

    public Candy() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getEnergy() {
        return energy;
    }

    public void setEnergy(int energy) {
        this.energy = energy;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Ingredient getIngredients() {
        return ingredients;
    }

    public void setIngredients(Ingredient ingredient) {
        this.ingredients = ingredient;
    }

    public Value getValue() {
        return value;
    }

    public void setValue(Value value) {
        this.value = value;
    }

    public String getProduction() {
        return production;
    }

    public void setProduction(String production) {
        this.production = production;
    }

    @Override
    public String toString() {
        return "Candy{" +
                "name='" + name + '\'' +
                ", type=" + type +
                ", ingredient=" + ingredients +
                ", value=" + value +
                ", production='" + production + '\'' +
                '}';
    }
}

package com.rybak.model;

import java.util.Comparator;

public class NameComparator implements Comparator<Candy> {

    @Override
    public int compare(Candy c1, Candy c2) {
        return c1.getName().compareTo(c2.getName());
    }
}

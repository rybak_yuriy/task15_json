package com.rybak.model;

import java.util.List;

public class Candies {

    private List<Candy> candy;

    public Candies() {
    }

    public List<Candy> getCandy() {
        return candy;
    }

    public void setCandy(List<Candy> candy) {
        this.candy = candy;
    }

    @Override
    public String toString() {
        return "Candies{" +
                "candy=" + candy +'\n'+
                '}';
    }
}

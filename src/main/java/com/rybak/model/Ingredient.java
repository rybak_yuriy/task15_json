package com.rybak.model;

public class Ingredient {

    private int water;
    private int sugar;
    private int fructose;
    private int vanillin;

    public Ingredient() {
    }

    public void setWater(int water) {
        this.water = water;
    }

    public void setSugar(int sugar) {
        this.sugar = sugar;
    }

    public void setFructose(int fructose) {
        this.fructose = fructose;
    }

    public void setVanillin(int vanillin) {
        this.vanillin = vanillin;
    }

    @Override
    public String toString() {
        return "Ingredient{" +
                "water=" + water +
                ", sugar=" + sugar +
                ", fructose=" + fructose +
                ", vanillin=" + vanillin +
                '}';
    }
}

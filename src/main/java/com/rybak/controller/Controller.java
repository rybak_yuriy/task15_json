package com.rybak.controller;

import com.rybak.parser.GsonParser;
import com.rybak.parser.JacksonParser;

public class Controller {

    JacksonParser jacksonParser;
    GsonParser gsonParser;

    public Controller() {
        jacksonParser = new JacksonParser();
        gsonParser = new GsonParser();
    }

    public void readByJackson() {
        jacksonParser.run();
    }

    public void readByGson() {
        gsonParser.run();
    }

}

package com.rybak.view;

import com.rybak.controller.Controller;

import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Scanner;

public class MyView {

    Scanner scanner = new Scanner(System.in);
    Controller controller;
    private Map<String, String> menu;
    private Map<String, Printable> menuMethods;

    public MyView() {
        controller = new Controller();
        menu = new LinkedHashMap<>();
        menuMethods = new LinkedHashMap<>();

        menu.put("1", "1 - Task 1 - Parse by Jackson");
        menu.put("2", "2 - Task 2 - Parse by Gson");
        menu.put("Q", "Q - Exit");

        menuMethods.put("1", this::doParseByJackson);
        menuMethods.put("2", this::doParseByGson);
    }

    private void doParseByJackson() {
        controller.readByJackson();
    }

    private void doParseByGson() {
        controller.readByGson();
    }

    private void outputMenu() {
        System.out.println("\nMENU:");
        menu.values().forEach(System.out::println);
    }

    public void start() {
        String keyMenu;
        do {
            outputMenu();
            System.out.print("Please, select menu point: ");
            keyMenu = scanner.nextLine().toUpperCase();
            try {
                menuMethods.get(keyMenu).print();
            } catch (Exception e) {
            }
        } while (!keyMenu.equals("Q"));
        scanner.close();
        System.exit(0);
    }

}

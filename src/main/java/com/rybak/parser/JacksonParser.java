package com.rybak.parser;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.rybak.model.Candies;
import com.rybak.model.Candy;
import com.rybak.model.JsonValidator;
import com.rybak.model.NameComparator;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.File;
import java.io.IOException;
import java.util.List;

public class JacksonParser {
    private static Logger log = LogManager.getLogger(JacksonParser.class);
    private final String validator = "src/main/resources/candiesSchema.json";
    private final String json = "src/main/resources/candies.json";
    ObjectMapper mapper;

    public JacksonParser() {
        mapper = new ObjectMapper();
    }

    public void run() {
        try {
            if (JsonValidator.validate(validator, json)) {
                File jsonFile = new File(json);
                Candies candies = mapper.readValue(jsonFile, Candies.class);
                NameComparator nameComparator = new NameComparator();
                List<Candy> sorted = candies.getCandy();
                sorted.sort(nameComparator);
                for (Candy candy : sorted) {
                    log.info(candy);
                }
            }
        } catch (IOException e) {
            log.error(e);
        }
    }

}

package com.rybak.parser;

import com.google.gson.Gson;
import com.rybak.model.Candies;
import com.rybak.model.Candy;
import com.rybak.model.JsonValidator;
import com.rybak.model.NameComparator;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.List;

public class GsonParser {

    private static Logger log = LogManager.getLogger(GsonParser.class);
    private final String validator = "src/main/resources/candiesSchema.json";
    private final String json = "src/main/resources/candies.json";
    Gson gson;

    public GsonParser() {
        gson = new Gson();
    }

    public void run() {
        try {
            if (JsonValidator.validate(validator, json)) {
                Candies candies = gson.fromJson(new FileReader(json), Candies.class);
                NameComparator nameComparator = new NameComparator();
                List<Candy> sorted = candies.getCandy();
                sorted.sort(nameComparator);
                for (Candy candy : sorted) {
                    log.info(candy);
                }
            }
        } catch (FileNotFoundException e) {
            log.error(e);
        }
    }

}
